const Koa = require("koa");
const Router = require("koa-router");
const Axios = require("axios");

const app = new Koa();
const router = new Router();

let coinData;

app.use(require("koa-body")());
app.use(router.allowedMethods());
app.use(router.routes());

// fetch coin data from external api then listen port 3000 

Axios.get("https://api.coinmarketcap.com/v1/ticker/")
    .then((response) => {
        coinData = response.data;

        app.listen(3000, () => {
            console.log("Example app listening on port 3000!")
        });

    })
    .catch((error) => {
        console.log(error);
    });

// create our api
router.get("/coin", (ctx) => {

    return new Promise((resolve, reject) => {
        getCoin()
            .then(() => {
                let controller = false;
    
                coinData.forEach((element) => {
                       
                    //provide case-insensitive search
                    let param = ctx.request.query.search;
                    let searcher = param.toLowerCase();
                    let coinname = element.name.toLowerCase();
                    let coinsymbol = element.symbol.toLowerCase();
                      
                    if (searcher === coinname || searcher === coinsymbol){
                         
                        ctx.body = {"status": "success", "price": element.price_usd, "coinsymbol": element.symbol };
                        ctx.status = 200;
                        controller = true;
                
                    }
                        
                });
                if (controller === false){
                    ctx.body = {"status": "error" };
                    ctx.status = 404;
                
                }
                return resolve();
            })
            .catch((err) => {
                
                return reject(new Error(err));
            });
    });
    
});

async function getCoin() {
    try {
        const response = await Axios.get("https://api.coinmarketcap.com/v1/ticker/");
        coinData = response.data;
    }
    catch (error) {
        console.error(error);
    }
}


